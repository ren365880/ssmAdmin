﻿# Host: 127.0.0.1  (Version 5.7.20-log)
# Date: 2018-03-17 10:21:04
# Generator: MySQL-Front 6.0  (Build 1.163)


#
# Structure for table "system_role"
#

CREATE TABLE `system_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '角色名',
  `rules` text COMMENT '规则id集合',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='后台角色表';

#
# Data for table "system_role"
#

INSERT INTO `system_role` VALUES (1,'超级管理员','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22');

#
# Structure for table "system_rule"
#

CREATE TABLE `system_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '规则',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '规则名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '规则备注',
  `group_id` int(11) DEFAULT '0' COMMENT '规则组id',
  PRIMARY KEY (`id`),
  KEY `url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='后台规则表';

#
# Data for table "system_rule"
#

INSERT INTO `system_rule` VALUES (1,'/admin/index/index','后台首页','后台首页',1),(2,'/admin/index/welcome','右侧欢迎页','右侧欢迎页',1),(3,'/admin/role/index','角色列表','角色列表',2),(4,'/admin/role/add','添加角色','添加角色',2),(5,'/admin/role/edit','编辑角色','编辑角色',2),(6,'/admin/role/del','删除角色','删除角色',2),(7,'/admin/role/rules','角色权限','角色权限',2),(8,'/admin/rule/index','规则列表','规则列表',2),(9,'/admin/rule/add','添加规则','添加规则',2),(10,'/admin/rule/edit','编辑规则','编辑规则',2),(11,'/admin/rule/del','删除规则','删除规则',2),(12,'/admin/rulegroup/index','分组列表','分组列表',2),(13,'/admin/rulegroup/add','添加分组','添加分组',2),(14,'/admin/rulegroup/edit','编辑分组','编辑分组',2),(15,'/admin/rulegroup/del','删除分组','删除分组',2),(16,'/admin/user/index','用户列表','用户列表',2),(17,'/admin/user/add','添加用户','添加用户',2),(18,'/admin/user/photo','修改头像','修改头像',2),(19,'/admin/user/photoHandle','头像上传处理','头像上传处理',2),(20,'/admin/user/adminedit','管理员修改用户信息','管理员修改',2),(21,'/admin/user/edit','个人修改自己信息','个人修改',2),(22,'/admin/user/del','删除用户帐号','删除用户帐号',2);

#
# Structure for table "system_rule_group"
#

CREATE TABLE `system_rule_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT '分组名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='后台规则分组表';

#
# Data for table "system_rule_group"
#

INSERT INTO `system_rule_group` VALUES (1,'公共权限'),(2,'权限管理');

#
# Structure for table "system_user"
#

CREATE TABLE `system_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL DEFAULT '' COMMENT '用户名',
  `true_name` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `password` varchar(70) NOT NULL DEFAULT '' COMMENT '密码',
  `login_time` int(11) NOT NULL DEFAULT '0' COMMENT '登陆时间',
  `login_ip` varchar(15) DEFAULT NULL COMMENT '登录ip',
  `error_num` tinyint(3) NOT NULL DEFAULT '0' COMMENT '密码错误次数',
  `error_time` int(11) NOT NULL DEFAULT '0' COMMENT '密码错误时间',
  `login_num` int(11) NOT NULL DEFAULT '1' COMMENT '登录次数',
  `role_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色id',
  `phone` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `photo` varchar(150) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#
# Data for table "system_user"
#

INSERT INTO `system_user` VALUES (1,'admin','任振星','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',1521250920,'127.0.0.1',0,0,4,1,'18315878931','/upload/2018/3/17/86aa41620db349058648567fb7c2dcb9.png');
