package com.ren.entity;

import java.util.List;

public class SystemRuleGroup {
    private Integer id;

    private String name;
    
    private List<SystemRule> rules;

    public List<SystemRule> getRules() {
		return rules;
	}

	public void setRules(List<SystemRule> rules) {
		this.rules = rules;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
}