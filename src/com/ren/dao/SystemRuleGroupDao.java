package com.ren.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ren.entity.SystemRuleGroup;

public interface SystemRuleGroupDao {
	List<SystemRuleGroup> queryGroupPage(@Param("start") int start,@Param("length") int length,@Param("name") String name,@Param("orderBy") String orderBy,@Param("search") String search);
	int countAll();
	void addRuleGroup(@Param("name") String name);
	int countSearch(@Param("search") String search);
	SystemRuleGroup queryByid(int id);
	int updateNameById(@Param("name") String name,@Param("id") int id);
	int deleteById(int id);
	List<SystemRuleGroup> queryAll();
	List<SystemRuleGroup> queryAllRules();
}
