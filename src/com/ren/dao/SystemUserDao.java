package com.ren.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.ren.entity.SystemUser;

public interface SystemUserDao {
	
	SystemUser queryById(int id);
	
	SystemUser queryByNameAndPassword(@Param("name") String name, @Param("password") String password);
	
	int updateUserLoginSuccessInfo(@Param("loginTime") int loginTime,@Param("loginIp") String loginIp,@Param("id") int id);
	
	SystemUser queryByUserName(String userName);
	
	int updateUserLoginErrorInfo(@Param("errorTime") int errorTime,@Param("id") int id);
	
	int updateUsePhoto(@Param("photo") String photo, @Param("id") int id);
	
	int updateUseInfo(@Param("trueName") String trueName,@Param("phone") String phone,@Param("password") String password,@Param("id") int id);
	
	int addUser(SystemUser user);
	
	List<SystemUser> queryUserPage(@Param("name") String name,@Param("orderBy") String orderBy,
			@Param("start") int start,@Param("length") int length,@Param("search") String search);
	int countAll();
	int countSearch(@Param("search") String search);
	
	int updateUserFromAdmin(SystemUser user);
	int deleteById(int id);
}
