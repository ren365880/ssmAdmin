package com.ren.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PostMapping;

import com.ren.entity.SystemRole;

public interface SystemRoleDao {
	int addRole(SystemRole role);
	List<SystemRole> queryRolePage(@Param("name") String name,@Param("orderBy") String orderBy,
			@Param("start") int start,@Param("length") int length,@Param("search") String search);
	int countSearch(@Param("search") String search);
	int countAll();
	SystemRole queryById(int id);
	int updateById(SystemRole role);
	int deleteById(int id);
	List<SystemRole> queryAll();
	
}
