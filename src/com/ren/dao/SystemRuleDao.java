package com.ren.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ren.entity.SystemRule;

public interface SystemRuleDao {
	int addRule(SystemRule rule);
	List<SystemRule> queryRulePage(@Param("name") String name,@Param("orderBy") String orderBy,
			@Param("start") int start,@Param("length") int length,@Param("search") String search);
	int countAll();
	int countSearch(@Param("search") String search);
	SystemRule queryById(int id);
	int updateById(@Param("SystemRule") SystemRule rule,@Param("id") int id);
	int deleteById(int id);
	List<SystemRule> queryByGroupId(@Param("groupId") int groupId);
	List<SystemRule> ruleListByIds(@Param("ids") String ids);
}
