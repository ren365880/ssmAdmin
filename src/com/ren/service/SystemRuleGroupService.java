package com.ren.service;

import java.util.List;

import com.ren.entity.SystemRuleGroup;

public interface SystemRuleGroupService {
	
	List<SystemRuleGroup> findGroupPage(int start,int length,String name,String orderBy,String search);
	
	int countAll();
	
	void addRuleGroup(String name);
	
	int countSearch(String search);
	
	SystemRuleGroup findById(int id);
	
	int updateNameById(String name,int id);
	
	int deleteById(int id);
	
	List<SystemRuleGroup> findAll();
	List<SystemRuleGroup> findAllRules();
}
