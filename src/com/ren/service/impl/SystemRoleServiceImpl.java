package com.ren.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ren.dao.SystemRoleDao;
import com.ren.entity.SystemRole;
import com.ren.service.SystemRoleService;

@Service
public class SystemRoleServiceImpl implements SystemRoleService{
	
	@Autowired
	SystemRoleDao roleDao;
	
	@Override
	public int addRole(SystemRole role) {
		return roleDao.addRole(role);
	}

	@Override
	public List<SystemRole> findRolePage(String name, String orderBy, int start, int length, String search) {
		return roleDao.queryRolePage(name, orderBy, start, length, search);
	}

	@Override
	public int countSearch(String search) {
		return roleDao.countSearch(search);
	}

	@Override
	public int countAll() {
		return roleDao.countAll();
	}

	@Override
	public SystemRole findById(int id) {
		return roleDao.queryById(id);
	}

	@Override
	public int updateById(SystemRole role) {
		return roleDao.updateById(role);
	}

	@Override
	public int deleteById(int id) {
		return roleDao.deleteById(id);
	}

	@Override
	public List<SystemRole> findAll() {
		return roleDao.queryAll();
	}

	

}
