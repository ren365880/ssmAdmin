package com.ren.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ren.dao.SystemRuleGroupDao;
import com.ren.entity.SystemRuleGroup;
import com.ren.service.SystemRuleGroupService;

@Service
public class SystemRuleGroupServiceImpl implements SystemRuleGroupService {
	@Autowired
	SystemRuleGroupDao ruleGroupDao;

	@Override
	public List<SystemRuleGroup> findGroupPage(int start, int length,String name,String orderBy,String search) {
		return ruleGroupDao.queryGroupPage(start, length,name,orderBy,search);
	}

	@Override
	public int countAll() {
		return ruleGroupDao.countAll();
	}

	@Override
	public void addRuleGroup(String name) {
		ruleGroupDao.addRuleGroup(name);
	}

	@Override
	public int countSearch(String search) {
		return ruleGroupDao.countSearch(search);
	}

	@Override
	public SystemRuleGroup findById(int id) {
		return ruleGroupDao.queryByid(id);
	}

	@Override
	public int updateNameById(String name, int id) {
		return ruleGroupDao.updateNameById(name, id);
	}

	@Override
	public int deleteById(int id) {
		return ruleGroupDao.deleteById(id);
	}

	@Override
	public List<SystemRuleGroup> findAll() {
		return ruleGroupDao.queryAll();
	}

	@Override
	public List<SystemRuleGroup> findAllRules() {
		return ruleGroupDao.queryAllRules();
	}

}
