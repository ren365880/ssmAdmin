package com.ren.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ren.dao.SystemRuleDao;
import com.ren.entity.SystemRule;
import com.ren.service.SystemRuleService;

@Service
public class SystemRuleServiceImpl implements SystemRuleService{
	
	@Autowired
	SystemRuleDao ruleDao;
	
	@Override
	public int addRule(SystemRule rule) {
		return ruleDao.addRule(rule);
	}

	@Override
	public List<SystemRule> findRulePage(String name, String orderBy, int start, int length, String search) {
		return ruleDao.queryRulePage(name, orderBy, start, length, search);
	}

	@Override
	public int countAll() {
		return ruleDao.countAll();
	}

	@Override
	public int countSearch(String search) {
		return ruleDao.countSearch(search);
	}

	@Override
	public SystemRule findById(int id) {
		return ruleDao.queryById(id);
	}

	@Override
	public int updateById(SystemRule rule, int id) {
		return ruleDao.updateById(rule, id);
	}

	@Override
	public int deleteById(int id) {
		return ruleDao.deleteById(id);
	}

	@Override
	public List<SystemRule> findByGroupId(int groupId) {
		return ruleDao.queryByGroupId(groupId);
	}

	@Override
	public List<SystemRule> ruleListByIds(String ids) {
		return ruleDao.ruleListByIds(ids);
	}

}
