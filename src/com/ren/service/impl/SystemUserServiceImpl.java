package com.ren.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ren.dao.SystemUserDao;
import com.ren.entity.SystemUser;
import com.ren.service.SystemUserService;

@Service
public class SystemUserServiceImpl implements SystemUserService {
	
	@Autowired
	SystemUserDao userDao;

	@Override
	public SystemUser findById(int id) {
		return userDao.queryById(id);
	}

	@Override
	public SystemUser findByNameAndPassword(String name, String password) {
		return userDao.queryByNameAndPassword(name, password);
	}

	@Override
	public int updateUserLoginSuccessInfo(int loginTime, String loginIp, int id) {
		return userDao.updateUserLoginSuccessInfo(loginTime, loginIp, id);
	}

	@Override
	public SystemUser findByUserName(String userName) {
		return userDao.queryByUserName(userName);
	}
	
	@Override
	public int updateUserLoginErrorInfo(int errorTime, int id) {
		return userDao.updateUserLoginErrorInfo(errorTime, id);
	}

	@Override
	public int updateUsePhoto(String photo, int id) {
		return userDao.updateUsePhoto(photo, id);
	}

	@Override
	public int updateUseInfo(String trueName, String phone, String password, int id) {
		return userDao.updateUseInfo(trueName, phone, password, id);
	}

	@Override
	public int addUser(SystemUser user) {
		return userDao.addUser(user);
	}

	@Override
	public List<SystemUser> findUserPage(String name, String orderBy, int start, int length, String search) {
		return userDao.queryUserPage(name, orderBy, start, length, search);
	}

	@Override
	public int countAll() {
		return userDao.countAll();
	}

	@Override
	public int countSearch(String search) {
		return userDao.countSearch(search);
	}

	@Override
	public int updateUserFromAdmin(SystemUser user) {
		return userDao.updateUserFromAdmin(user);
	}

	@Override
	public int deleteById(int id) {
		return userDao.deleteById(id);
	}

}
