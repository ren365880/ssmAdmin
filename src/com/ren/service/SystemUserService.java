package com.ren.service;

import java.util.List;

import com.ren.entity.SystemUser;

public interface SystemUserService {
	
	SystemUser findById(int id);
	
	SystemUser findByNameAndPassword(String name,String password);
	
	int updateUserLoginSuccessInfo(int loginTime,String loginIp,int id);
	
	SystemUser findByUserName(String userName);
	
	int updateUserLoginErrorInfo(int errorTime,int id);
	
	int updateUsePhoto(String photo,int id);
	
	int updateUseInfo(String trueName,String phone,String password,int id);
	
	int addUser(SystemUser user);
	
	List<SystemUser> findUserPage(String name,String orderBy,int start,int length,String search);
	int countAll();
	int countSearch(String search);
	int updateUserFromAdmin(SystemUser user);
	int deleteById(int id);
}
