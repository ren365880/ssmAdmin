package com.ren.service;

import java.util.List;

import com.ren.entity.SystemRole;

public interface SystemRoleService {
	int addRole(SystemRole role);
	List<SystemRole> findRolePage(String name,String orderBy,int start,int length,String search);
	int countSearch(String search);
	int countAll();
	SystemRole findById(int id);
	int updateById(SystemRole role);
	int deleteById(int id);
	List<SystemRole> findAll();
	
}
