package com.ren.service;

import java.util.List;

import com.ren.entity.SystemRule;

public interface SystemRuleService {
	int addRule(SystemRule rule);
	List<SystemRule> findRulePage(String name,String orderBy,int start,int length,String search);
	int countAll();
	int countSearch(String search);
	SystemRule findById(int id);
	int updateById(SystemRule rule,int id);
	int deleteById(int id);
	List<SystemRule> findByGroupId(int groupId);
	List<SystemRule> ruleListByIds(String ids);
}
